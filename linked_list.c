#include <stdlib.h>
#include <stdio.h>
#include "linked_list.h"

struct Node* list_create() {
    struct Node* root = malloc(sizeof(struct Node));
    root->value = 0;
    root->next = NULL;
    return root;
}

struct Node* list_create_wvalue(int value) {
    struct Node* node= malloc(sizeof(struct Node*));
    node->value = value;
    node->next = NULL;
    return node;

}

struct Node* list_add_front(int value, struct Node** list) {
    struct Node* newRoot = malloc(sizeof(struct Node));
    newRoot->value = value;
    newRoot->next = *list;
    return newRoot;
}

struct Node* list_add_back(int value, struct Node** list) {
    struct Node* nextNode = *list;
    while (1) {
        if (nextNode->next == NULL) break;
        nextNode = nextNode->next;
    }
    struct Node* newNode = malloc(sizeof(struct Node));
    newNode->value = value;
    newNode->next = NULL;
    nextNode->next = newNode;
    return *list;
}

void print_list(struct Node** list) {
    struct Node* nextNode = *list;
    printf("%d ", nextNode->value);
    while ((nextNode = (nextNode)->next) != NULL) {
        printf("%d ", nextNode->value);
    }
    printf("\n");
}

int list_get(int index, struct Node** list) {
    if (index < 0) return 0;
    struct Node* nextNode = *list;
    int i = 0;
    while (1) {
        if (nextNode->next == NULL) break;
        if (i == index) break;
        nextNode = nextNode->next;
        i += 1;
    }
    if (i != index) return 0;
    return nextNode->value;
}

void list_free(struct Node** list) {
    struct Node* nextNode = *list;
    while (1) {
        if (nextNode->next == NULL) break;
        struct Node* previousNode = nextNode;
        nextNode = nextNode->next;
        free(previousNode);
    }
}

int list_length(struct Node** list) {
    struct Node* nextNode = *list;
    int len = 1;
    while (1) {
        if (nextNode->next == NULL) break;
        nextNode = nextNode->next;
        len += 1;
    }
    return len;
}

struct Node* list_node_at(struct Node** list, int index) {
    if (index < 0) return NULL;
    struct Node* nextNode = *list;
    int i = 0;
    while (1) {
        if (nextNode->next == NULL) break;
        if (i == index) break;
        nextNode = nextNode->next;
        i += 1;
    }
    if (i != index) return NULL;
    return nextNode;
}

int list_sum(struct Node** list) {
    struct Node* nextNode = *list;
    int sum = nextNode->value;
    while (1) {
        if (nextNode->next == NULL) break;
        nextNode = nextNode->next;
        sum += nextNode->value;
    }
    return sum;
}

void foreach(struct Node** list, void (* func)(int)) {
    struct Node* nextNode = *list;
    while (1) {
        if (nextNode->next == NULL) break;
        func(nextNode->value);
        nextNode = nextNode->next;
    }
}

struct Node* map(struct Node* list, int (* func)(int)) {
    struct Node* nextNode = list;
    struct Node* newList = list_create();

    while (1) {
        if (nextNode->next == NULL) break;
        newList = list_add_back(func(nextNode->value), &newList);
        nextNode = nextNode->next;
    }

    return newList;
}

struct Node* map_mut(struct Node** list, int (* operator)(int)) {
    struct Node* nextNode = *list;

    while (1) {
        if (nextNode->next == NULL) break;
        nextNode->value = operator(nextNode->value);
        nextNode = nextNode->next;
    }

    return *list;
}

int foldl(struct Node** list, int acc, int (* operator)(int, int)) {
    struct Node* nextNode = *list;

    while (1) {
        if (nextNode->next == NULL) break;
        acc = operator(nextNode->value, acc);
        nextNode = nextNode->next;
    }

    return acc;
}

struct Node* iterate(int value, size_t length, int (* operator)(int)) {
    size_t i;
    struct Node* list;

    list = list_create();

    for (i = 1; i < length; i++) {
        list_add_back(value, &list);
        value = operator(value);
    }

    return list;
}

struct Node* list_add_after(struct Node *node, int value) {
    struct Node* new_node = list_create_wvalue(value);
    if (node != NULL) {
        new_node->next = node->next;
        node->next = new_node;
    }
    return new_node;
}


int save(struct Node* list, const char* filename) {
    struct Node* iter;
    FILE* f;
    errno = 0;
    f = fopen(filename, "w");
    if (errno) return 0;

    for (iter = list; iter != NULL; iter = iter->next) {
        fprintf(f, "%d ", iter->value);
        if (errno || ferror(f)) {
            fclose(f);
            return 0;
        }
    }

    fclose(f);
    if (errno) return 0;
    return 1;
}

int load(struct Node** list, const char* filename) {
    struct Node* iter = NULL, * start = NULL;
    int value;
    FILE* f;
    errno = 0;
    f = fopen(filename, "r");
    if (errno) return 0;

    while (1) {
        fscanf(f, "%d", &value);
        if (feof(f)) break;

        if (errno || ferror(f)) {
            fclose(f);
            return 0;
        }

        iter = list_add_after(iter, value);
        if (start == NULL) start = iter;
    }

    *list = start;
    fclose(f);
    if (errno) return 0;
    return 1;
}

int serialize(struct Node* list, const char* filename) {
    struct Node* iter;
    FILE* f;
    errno = 0;
    f = fopen(filename, "wb");
    if (errno) return 0;

    for (iter = list; iter != NULL; iter = iter->next) {
        fwrite(&iter->value, sizeof(int), 1, f);
        if (errno || ferror(f)) {
            fclose(f);
            return 0;
        }
    }

    fclose(f);
    if (errno) return 0;
    return 1;
}

int deserialize(struct Node** list, const char* filename) {
    struct Node* iter = NULL, * start = NULL;
    int value;
    FILE* f;
    errno = 0;
    f = fopen(filename, "r");
    if (errno) return 0;

    while (1) {
        fread(&value, sizeof(int), 1, f);
        if (feof(f)) break;

        if (errno || ferror(f)) {
            fclose(f);
            return 0;
        }

        iter = list_add_after(iter, value);
        if (start == NULL) start = iter;
    }

    *list = start;
    fclose(f);
    if (errno) return 0;
    return 1;
}

