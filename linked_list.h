#pragma once

#include <errno.h>

struct Node {
    struct Node* next;
    int value;
};


struct Node* list_create();

struct Node* list_add_front(int value, struct Node** list);

struct Node* list_add_back(int value, struct Node** list);

int list_get(int index, struct Node** list);

void list_free(struct Node** list);

int list_length(struct Node** list);

struct Node* list_node_at(struct Node** list, int index);

int list_sum(struct Node** list);

void print_list(struct Node** list);

void foreach(struct Node** list, void (* func)(int));

struct Node* map(struct Node* list, int (* operator)(int));

struct Node* map_mut(struct Node** list, int (* operator)(int));

int foldl(struct Node** list, int acc, int (* operator)(int, int));

struct Node* iterate(int value, size_t length, int (* operator)(int));

int save(struct Node* list, const char* filename);

int load(struct Node **list, const char *filename);

int serialize(struct Node* list, const char* filename);

int deserialize(struct Node** list, const char* filename);