#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "linked_list.h"

void print(int element) {
    printf("%d ", element);
}

void print_newline(int element) {
    printf("%d\n", element);
}

int square(int element) {
    return element * element;
}

int cube(int el) {
    return el * el * el;
}

int module(int el) {
    return el >= 0 ? el : -el;
}

int sum(int value, int acc) {
    return value + acc;
}

int max(int value, int max) {
    return value > max ? value : max;
}

int min(int value, int min) {
    return value < min ? value : min;
}

int pow_2(int el) {
    return el * 2;
}

int main() {
    int value;
    struct Node* list = list_create();

    while (1) {
        scanf("%d", &value);
        if (value == '\n') break;
        list = list_add_front(value, &list);
    }

    struct Node* newList = map(list, square);
    struct Node* newList1 = map(list, cube);

//    print_list(&newList);
//    print_list(&newList1);

    list = map_mut(&list, module);

//    print_list(&list);

    int sum_acc = 0;
    sum_acc = foldl(&list, sum_acc, sum);
    printf("Sum: %d\n", sum_acc);

    int max_acc = foldl(&list, max_acc, max);
//    printf("Max: %d\n", max_acc);

    int min_acc = foldl(&list, min_acc, min);
//    printf("Min: %d\n", min_acc);

    struct Node* newList2 = iterate(1, 4, pow_2);
//    print_list(&newList2);

    if(!save(list, "list.txt")) {
        exit(EXIT_FAILURE);
    }

    struct Node* loadedList = NULL;

    load(&loadedList, "list.txt");

    print_list(&loadedList);

    list_free(&list);
    list_free(&newList);
    list_free(&newList1);
    list_free(&newList2);
    list_free(&loadedList);
    return 0;
}
